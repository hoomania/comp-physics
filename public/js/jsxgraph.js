var board = JXG.JSXGraph.initBoard('fig_one', {
  boundingbox: [-20, 20, 20, -20],
  axis: true,
  grid: true
});

var array = [
  [0, -1],
  [1, 2],
  [2, 5],
  [3, 8],
  [-1, -4],
  [-2, -7],
  [-3, -10]
];

for (i = 0; i < array.length; i++) {

  board.create('point', [array[i][0], array[i][1]], {
    name: '',
    size: 1
  });
}

var board = JXG.JSXGraph.initBoard('fig_two', {
  boundingbox: [-20, 20, 20, -20],
  axis: true,
  grid: true
});

board.create('line', [
  [-3, -10],
  [3, 8]
]);


var board = JXG.JSXGraph.initBoard('fig_five', {
  boundingbox: [-20, 20, 20, -20],
  axis: true,
  grid: true
});

board.create('line', [
  [-1, -1],
  [1, 1]
]);

var array = [
  [15, 10],
  [12, 15],
  [7, 4],
  [0, 5],
  [-2, -5],
  [-4, -8],
  [-8, -11]
];
for (i = 0; i < array.length; i++) {

  board.create('point', [array[i][0], array[i][1]], {
    name: '',
    size: 1
  });

  board.create('line', [
    [array[i][0], array[i][1]],
    [array[i][0], array[i][0]]
  ], {
    straightFirst: false,
    straightLast: false,
    strokeColor: '#954001',
    strokeWidth: 2,
    dash: 2
  });
}
